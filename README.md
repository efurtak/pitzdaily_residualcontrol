This casefile illustrates monitoring and control of the pimpleFoam solver using the initial residuals.

To run:

blockMesh

pimpleFoam > log

gnuplot residuals

![Alt text](residuals.png )